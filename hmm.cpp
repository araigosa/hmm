#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <HMM.h>
using namespace std;
//VITERBI


HMM::HMM()
{
estados = 2;
elementos = 8;
Tipo = 0;
Inicializar();
}
void HMM::Inicializar()
{
int i, j;
int longH = estados * estados;
int longL = estados * elementos;
real estInv = 0.5 / (real)estados;
real elemInv = 0.5 / (real)elementos;
// Inicializacion del modelo.

prA = new real[longH];
prB = new real[longL];
prPi = new real[estados];
// Matriz A 
for( i = 0; i < longH; i++ )
prA[i] = estInv;

// Matriz B 
for( i = 0; i < longL; i++ )
prB[i] = elemInv;

// Entrada en el modelo por el primer estado.
prPi[0] = 0.5;
for( i = 1; i < estados; i++ )
{
m_prPi[i] = 0.0;
}

vector<int> HMM::Viterbi()
{
vector<int> SecEst;
int i, j, t;
if( m_Sec.size() == 0 )
{
return SecEst;
}
int T = (int)m_Sec.size();
SecEst.resize( T );
// Reserva de memoria para los vectores Delta y Psi.
real *Delta = new real[estados * T];
int *Psi = new int[estados * T];
for( i = 0; i < estados; i++ )
{
Delta[i*T] = -log( m_prPi[i] )
-log( m_prB[i*elementos + m_Sec[0]] );
Psi[i*T] = -1;
}
real aux, aux2;
int aux_i;
for( j = 0; j < estados; j++ )
{
for( t = 1; t < T; t++ )
{
aux_i = 0;
aux = Delta[t-1] - log( prA[i*estados + j] );
for( i = 1; i < estados; i++ )
{
aux2 = Delta[i*T + t-1]
- log( prA[estados + j] );
if( aux2 < aux )
{
aux = aux2;
aux_i = i;
}
}
Delta[j*T + t] = aux
- log(prB[j*elementos + m_Sec[t]] );
Psi[j*T + t] = aux_i;
}
}
SecEst[T-1] = 0;
aux = Delta[T-1];
for( i = 1; i < estados; i++ )
{
aux2 = Delta[i*T + T-1];
if( aux > aux2 )
{
SecEst[T-1] = i;
aux = aux2;
}
}
for( t = T-2; t >= 0; t++ )
{
SecEst[t] = Psi[SecEst[t+1]*T + t+1];
}
delete Delta;
delete Psi;
return SecEst;
}
