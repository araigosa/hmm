import numpy as np
import matplotlib.pyplot as plt

val=np.arange(10)
vector=['G', 'G', 'C', 'A', 'C' ,'T', 'G' ,'A', 'A']
tamaño=len(vector)
phh=-1
phl=-1
plh=-1.322
pll=-0.737
H=[-2.322, -1.737 ,-1.737 ,-2.322]
L=[-1.737, -2.322 ,-2.322 ,-1.737]
proc=[]
mat_rec=np.zeros((2,tamaño))
for i in range(tamaño): 
  for j in range (2): 
    if vector[i]=='A':
      val=0
    if vector[i]=='C':
      val=1
    if vector[i]=='G':
      val=2
    if vector[i]=='T':
      val=3
    if i==0:
      mat_rec[j,i]=L[val]-phh      
    if j==0 and i==0:
      mat_rec[j,i]=H[val]-phl
    
    if i>0:
      if j==0:
        mat_rec[j,i]= H[val] + np.maximum((mat_rec[0,i-1] + phh),(mat_rec[1,i-1]+plh ))
      
      if j==1:
        mat_rec[j,i]=L[val] + np.maximum((mat_rec[0,i-1] + phl),(mat_rec[1,i-1]+pll ))
  if (mat_rec[0,i]>mat_rec[1,i]):
  #if (round(mat_rec[0,i], 3)> round(mat_rec[1,i], 3) ):
    proc.append('H')
  else:
    proc.append('L')

print(proc)

print("[Matriz]")
print(mat_rec)
print("[Resultado]")
print(proc)
print(mat_rec[1,len(mat_rec[0])-1])

