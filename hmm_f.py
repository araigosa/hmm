import numpy as np
from math import log
vector=['G','G','C','A']
tamaño=len(vector)
phh=-1
phl=-1
plh=-1.322
pll=-0.737
H=[-2.322,-1.737,-1.737,-2.322]
L=[-1.737,-2.322,-2.322,-1.737]
mat_rec=np.zeros((2,tamaño))
for i in range(tamaño):
    for j in range (2):
        if vector[i]=='A':
            val=0
        if vector[i]=='C':
            val=1
        if vector[i]=='G':
            val=2
        if vector[i]=='T':
            val=3
        if i==0:
            mat_rec[j,i]=L[val] + phh
        if j==0 and i==0:
            mat_rec[j,i]=H[val] + phl
        if i>0:
            if j == 0:
                a = mat_rec[0,i-1] + phh +  H[val]
                b = mat_rec[1,i-1] + H[val]+ plh
                mat_rec[j,i]= log((2**a  +  2**b),2)
            if j == 1:
                a = mat_rec[1,i-1] + pll  +  L[val]
                b = mat_rec[0,i-1] + phh + L[val]
                mat_rec[j,i]= log(2**a  +  2**b, 2)
mat_rec = 2**mat_rec
print("[Matriz]")
print(mat_rec)
print("[Resultado]")
print(mat_rec[1,len(mat_rec[0])-1] + mat_rec[0,len(mat_rec[0])-1])

